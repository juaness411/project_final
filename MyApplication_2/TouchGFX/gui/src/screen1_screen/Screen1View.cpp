#include <gui/screen1_screen/Screen1View.hpp>

Screen1View::Screen1View()
{
	toggle_actuator = 0;
}

void Screen1View::setupScreen()
{
    Screen1ViewBase::setupScreen();
}

void Screen1View::tearDownScreen()
{
    Screen1ViewBase::tearDownScreen();
}

void Screen1View::updateTemp(uint8_t new_temp){
	Temperature.resizeToCurrentText();
	Temperature.invalidate();
	Unicode::snprintf(TemperatureBuffer, TEMPERATURE_SIZE, "%d", new_temp);
	Temperature.resizeToCurrentText();
	Temperature.invalidate();
}

void Screen1View::update_GPIO(){
	if(toggle_actuator == 0){
		HAL_GPIO_WritePin(GPIOD, actuator_Pin, GPIO_PIN_RESET);
		toggle_actuator = 1;
	}else if(toggle_actuator == 1){
		HAL_GPIO_WritePin(GPIOD, actuator_Pin, GPIO_PIN_SET);
		toggle_actuator = 0;
	}
}
