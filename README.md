TABLE OF CONTENTS

               1. Project description 
               2. Technologies
               3 .Repository developer data

1. Project description

-This will be a temperature reading system.
-There will be two interfaces, AWS website,  and STM32's LCD.
-The LCD will show the current temperature.
-The LCD will have a button that will turn  on/off a GPIO.
-The AWS website will show the current  temperature.

2. Technologies

ESP32:

-UART communication with the STM32  (UART parser). PIN RX2, TX2
-MQTT communication with AWS.

STM32F429I-DISC1:

-UART communication with ESP32 (UART parser). USART1: PA9 TX, PA10 RX
-Sensor readings. 
-Control LCD, and touch.
-Trigger actuators. (Pin PD11)

SENSOR BMP280:

-Read temperature.
SCL PIN PE2
SDA PIN PE6
CSB PIN PE4
SDO PIN PE5

3. Repository developer data

Juan Esteban Sarmiento Quintero
Email: juaness411@gmail.com